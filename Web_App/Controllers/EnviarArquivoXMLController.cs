﻿using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Web_App.Controllers
{
    public class EnviarArquivoXMLController : Controller
    {
        // GET: EnviarArquivoXml
        public ActionResult Index()
        {
            ViewBag.nome = "Teste Para GitFlow no VS2017";

            return View();
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Enviar(HttpPostedFileBase arquivo)
        {
            ViewBag.nome = "Teste Para GitFlow no VS2017";

            XmlDocument xml = new XmlDocument();
            xml.Load(arquivo.FileName);

            return View();
        }
    }
}